import React, { Component } from 'react';
import MyCars from './components/MyCars';
import './App.css';

class App extends Component {
  
  // state definis l'etat du composant
  // state = {
  //   titre: 'Mon catalogue de voitures !'
  // }

  // changeTitle = (e) => {
  //   // setState sert a changer le state la on le change via le state
  //   this.setState({
  //     titre: 'Mon nouveau titre'
  //   })

  // }
  // // on le change via le parametre
  // changeViaParam = (titre) => {
  //   this.setState({
  //     titre
  //   })
  // }
  // //on le change via le bind
  // changeViaBind = (param) => {
  //   this.setState({
  //     titre: param
  //   })
  // }
  // // on le change en direct dans l'input
  // changeViaInput = (e) => {
  //   this.setState ({
  //     titre: e.target.value
  //   })
    
  // }
  render() {
    return (
    <div className="App">
      <MyCars />

      {/* <button onClick={this.changeTitle}>changer le nom en dur</button>
      <button onClick={() => this.changeViaParam('titre via un parametre')}>via param</button>
      <button onClick={this.changeViaBind.bind(this, 'titre via bind')}>via bind</button>
      <input type="text" onChange={this.changeViaInput} value={this.state.titre}/> */}

    </div>
  );
  }
  
}

export default App;
