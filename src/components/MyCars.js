import React, { Component } from 'react';
import Car from './Cars';
import '../styles/MyCars.css'

class MyCars extends Component {

    state = {
        voitures:[
            {name:'ford', color:'red', year: 2000},
            {name:'mercedes', color:'black', year: 1958},
            {name:'peugeot', color:'blue', year: 2020}
        ],
        title: 'Mon catalogue voitures'
    }

    // ajout fonction qui fait une alert quand on copy !
    // noCopy = () => {
    //     alert('Copie pas mon texte BATARD !');
    // }
    // ajout fonction qui ajoute une classe css
    // addStyle = (e) => {
        
    //     e.target.classList.toggle('styled')
    // }

    addTenYears = () => {
        const updatedState = this.state.voitures.map((param) => {
            return param.year -= 10;
        })

        this.setState({
            updatedState
        })
    } 

    render() {
        
        const year = new Date().getFullYear();

        return (
            <div>
                {/* fonction hover  onMouseOver={this.addStyle}*/}
                <h1>{this.state.title}</h1>
                {/* fonction nocopy */}
                {/* <p onCopy={this.noCopy} >lorem ipsum dolor si amet</p> */}

                <button onClick={this.addTenYears}> + 10ans </button>

                {/* premiere methode */}
                {/* <Car year={year - this.state.voitures[0].year + ' ans'} color={this.state.voitures[0].color}>{this.state.voitures[0].name}</Car>
                <Car year={year - this.state.voitures[1].year + ' ans'} color={this.state.voitures[1].color}>{this.state.voitures[1].name}</Car>
                <Car year={year - this.state.voitures[2].year + ' ans'} color={this.state.voitures[2].color}>{this.state.voitures[2].name}</Car> */}

                    {/* deuxieme methode */}
                {   
                    this.state.voitures.map((voiture, index ) => {
                        return (
                            <div key={index}>
                            <Car name={voiture.name}year={year - voiture.year + ' ans'} color={voiture.color} />
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}


export default MyCars;