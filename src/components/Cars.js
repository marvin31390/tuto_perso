import React from 'react';
import "../styles/Cars.css";

const Car = ({name, color, year}) => {

    const colorInfo = color ? (<p>Couleur: {color}</p>) : (<p>Couleur : Néant</p>) ;

    if(name) {
        return(
            <div className="cars">
                <p>Marque: {name}</p>
                <p>Année: {year}</p>
                {colorInfo}
            </div>
        )
    }else {
        return null;
    }

    
}


export default Car;